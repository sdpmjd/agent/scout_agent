<launch>


  <arg name='twist_filter_enable' default='true' />

  <!--
    # costmap_generator #
    This node reads PointCloud and/or DetectedObjectArray and creates an OccupancyGrid and GridMap.
    In:  /points_no_ground [sensor_msgs::PointCloud2]
    In:  /prediction/motion_predictor/objects [autoware_msgs::DetectedObjectArray] 
    Out: /semantics/costmap [grid_map_msgs::GridMap]
    Out: /semantics/costmap_generator/occupancy_grid [nav_msgs::OccupancyGrid]
  -->
  <include file='$(find costmap_generator)/launch/costmap_generator.launch'>

    <arg name="grid_min_value"  value ="0.0" />
    <arg name="grid_max_value"  value ="1.0" />
    <arg name="grid_resolution" value ="0.2" />
    <arg name="grid_length_x"   value ="15" /> <!-- 50 -->
    <arg name="grid_length_y"   value ="6" /> <!-- 30 -->
    <arg name="grid_position_x" value ="7" /> <!-- 20 -->
    <arg name="grid_position_y" value ="0" />
    <arg name="maximum_lidar_height_thres" value ="0.3" />
    <arg name="minimum_lidar_height_thres" value ="-2.2" /> <!--2.2-->
    <arg name="expand_polygon_size" value = "1" /> <!--1-->
    <arg name="size_of_expansion_kernel" value ="9" /> <!--9-->

    <arg name='use_points'  value ='true' />
    <arg name='use_wayarea' value ='False' />
    <arg name='use_objects_box' value ='true' />
    <arg name='use_objects_convex_hull' value ='False' />

    <arg name='objects_input' value ='/prediction/motion_predictor/objects' />
    <!-- <arg name='points_input' value ='/detection/lidar_detector/cloud_clusters' /> -->
    <!-- <arg name='points_input' value ='/points_no_ground' /> -->
    <arg name='points_input' value ='/my_filtered_points' />
  </include>

  <!--
    # obstacle_avoid #
    In:  /base_waypoints [autoware_msgs/Lane]
    In:  /closest_waypoint [std_msgs/Int32]
    In:  /current_pose [geometry_msgs/PoseStamped]
    In:  /current_velocity [geometry_msgs/TwistStamped]
    In:  /obstacle_waypoint [std_msgs/Int32]
    In:  /semantics/costmap_generator/occupancy_grid [nav_msgs/OccupancyGrid]
    Out: /safety_waypoints [autoware_msgs::Lane]
  -->
  <include file='$(find waypoint_planner)/launch/astar_avoid.launch'>
    <arg name='enable_avoidance' default='true' />
    <arg name="avoid_waypoints_velocity" value="1.5"/> <!--5.0-->
    <arg name="avoid_start_velocity" value="1.0" />    <!--3.0-->
    <arg name="replan_interval" value="2.0" />         <!--2.0-->
    <arg name="search_waypoints_size" value="13" />    <!--50-->
    <arg name="search_waypoints_delta" value="1" />    <!--2.0-->
    <arg name="closest_search_size" value="12" />      <!--30.0-->

    <!-- A* search configurations -->
    <arg name="use_back" value="false" />
    <arg name="use_potential_heuristic" value="true" />
    <arg name="use_wavefront_heuristic" value="false" />
    <arg name="time_limit" value="1000.0" />
    <arg name="robot_length" value="0.93" />           <!--4.5-->
    <arg name="robot_width" value="0.699" />           <!--1.75-->
    <arg name="robot_base2back" value="0.1" />        <!--1.0-->
    <arg name="minimum_turning_radius" value="2.0" /> <!--6.0-->
    <arg name="theta_size" value="48" />              <!--48-->
    <arg name="curve_weight" value="1.2" />
    <arg name="reverse_weight" value="2.00" />
    <arg name="lateral_goal_range" value="0.5" />
    <arg name="longitudinal_goal_range" value="2.0" />
    <arg name="angle_goal_range" value="6.0" />       <!--6.0-->
    <arg name="obstacle_threshold" value="100" />
    <arg name="potential_weight" value="10.0" />
    <arg name="distance_heuristic_weight" value="1.0" />

  </include>

  <!--
    # velocity_set #
    In:  /current_pose [geometry_msgs::PoseStamped]
    In:  /current_velocity [geometry_msgs::TwistStamped]
    In:  /localizer_pose [geometry_msgs::PoseStamped]
    In:  /points_no_ground [sensor_msgs::PointCloud2]
    In:  /safety_waypoints [autoware_msgs::Lane]
    In:  /state/stopline_wpidx [std_msgs::Int32]
    In:  /vector_map_info/* [vector_map_msgs::*]
    Out: /detection_range [visualization_msgs::MarkerArray]
    Out: /final_waypoints [autoware_msgs::Lane]
    Out: /node_status [autoware_system_msgs::NodeStatus]
    Out: /obstacle [visualization_msgs::Marker]
    Out: /obstacle_waypoint [std_msgs::Int32]
    Out: /stopline_waypoint [std_msgs::Int32]
  -->

  <arg name='use_velocity_set' default='true' />
  <include file='$(find waypoint_planner)/launch/velocity_set.launch' if="$(arg use_velocity_set)">
    <arg name='use_crosswalk_detection' default='false' />
    <!-- <arg name='points_topic' default='points_no_ground' /> -->
    <arg name='points_topic' value ='/my_filtered_points' />
    <arg name='velocity_offset' default='2' />
    <arg name='decelerate_vel_min' default='1.3' />
    <arg name='remove_points_upto' default='0.5' />
    <arg name='enable_multiple_crosswalk_detection' default='false' />
    <arg name='stop_distance_obstacle' default='6.0' /> <!-- 10.0 -->
    <arg name='stop_distance_stopline' default='1.0' /> <!-- 5.0 -->
    <arg name='detection_range' default='0.8' /> <!--1.3-->
    <arg name='points_threshold' default='10' /> <!-- 10 -->
    <arg name='detection_height_top' default='2.2' />
    <arg name='detection_height_bottom' default='0.1' />
    <arg name='deceleration_obstacle' default='1.5' />
    <arg name='deceleration_stopline' default='0.6' />
    <arg name='velocity_change_limit' default='5' />
    <arg name='deceleration_range' default='0' />
    <arg name='temporal_waypoints_size' default='100' />
  </include>

  <!--
    # pure_pursuit #
    Follow a list of waypoints
    In:  /final_waypoints [autoware_msgs::Lane]
    In:  /current_pose [geometry_msgs::PoseStamped]
    In:  /current_velocity [geometry_msgs::TwistStamped]
    Out: /ctrl_raw [autoware_msgs::ControlCommandStamped]
    Out: /twist_raw [geometry_msgs::TwistStamped]
  -->

  <arg name="wheel_base" default="0.49" />
  <param name="vehicle_info/wheel_base" value="$(arg wheel_base)" />

  <node pkg='rostopic' type='rostopic' name='config_waypoint_follower_rostopic'
        args='pub -l /config/waypoint_follower autoware_config_msgs/ConfigWaypointFollower
        "{ header: auto, param_flag: 0, velocity: 2.0, lookahead_distance: 1.0, lookahead_ratio: 0.5, minimum_lookahead_distance: 1.2, displacement_threshold: 0.0, relative_angle_threshold: 0.0 }"'/>

  <!-- <node pkg='rostopic' type='rostopic' name='config_waypoint_follower_rostopic'
        args='pub -l /config/waypoint_follower autoware_config_msgs/ConfigWaypointFollower
        "{ header: auto, param_flag: 1, velocity: 1, lookahead_distance: 1.5, lookahead_ratio: 0.5, minimum_lookahead_distance: 1.2, displacement_threshold: 0.0, relative_angle_threshold: 0.0 }"'/>  -->

  <include file='$(find pure_pursuit)/launch/pure_pursuit.launch'/> 

  <!--
    # twist_filter #
    Launches twist_filter and twist_gate
    In:  /ctrl_raw [autoware_msgs::ControlCommandStamped]
    In:  /twist_raw [geometry_msgs::TwistStamped]
    Out: /vehicle_cmd [autoware_msgs::VehicleCmd] (the combined vehicle command for actuator)
  -->

  <include file='$(find twist_filter)/launch/twist_filter.launch' if="$(arg twist_filter_enable)">
    <arg name="wheel_base" default="$(arg wheel_base)" />
    <arg name="lateral_accel_limit" default="5.0" />
    <arg name="lateral_jerk_limit" default="5.0" />
    <arg name="lowpass_gain_linear_x" default="0.0" />
    <arg name="lowpass_gain_angular_z" default="0.0" />
    <arg name="lowpass_gain_steering_angle" default="0.0" />
  </include>

  <!-- 
    # waypoint visualizer #
    to visualize the waypoints in rviz
    In:  /closest_waypoint [std_msgs::Int32]
    In:  /final_waypoints [autoware_msgs::Lane]
    In:  /traffic_waypoints_array [autoware_msgs::LaneArray]
    In:  /lane_waypoints_array [autoware_msgs::LaneArray]
    Out: /global_waypoints_mark [visualization_msgs::MarkerArray]
    Out: /local_waypoionts_mark [visualization_msgs::MarkerArray]
  -->
  <node pkg='waypoint_maker' type='waypoint_marker_publisher' name='waypoint_marker_publisher' output='screen'/>
  <!-- 
    # waypoint velocity visualizer #
    to visualize the waypoints velocity in rviz
    In:  /current_pose [geometry_msgs::PoseStamped]
    In:  /current_velocity [geometry_msgs::TwistStamped]
    In:  /final_waypoints [autoware_msgs::Lane]
    In:  /lane_waypoints_array [autoware_msgs::LaneArray]
    In:  /twist_cmd [geometry_msgs::TwistStamped]
    Out: /waypoints_velocity [visualization_msgs::MarkerArray]
  -->
  <include file='$(find waypoint_maker)/launch/waypoint_velocity_visualizer.launch' />

</launch>
